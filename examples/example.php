<?php

require 'vendor/autoload.php';

use KarolSzarafinowski\Breadcrumb\Breadcrumb;
use KarolSzarafinowski\Breadcrumb\Item;

$bc = new Breadcrumb();
$bc->items->append(new Item('Offer', 'https://example.com/offer'));
$bc->items->append(new Item('Websites', 'https://example.com/offer/websites'));

echo $bc->getAsHtml();
