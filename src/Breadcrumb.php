<?php
/**
 * Created by PhpStorm.
 * User: eska
 * Date: 07.03.19
 * Time: 16:29
 */

namespace KarolSzarafinowski\Breadcrumb;

class Breadcrumb
{
    /**
     * @var \ArrayObject
     */
    public $items;

    public function __construct()
    {
        $this->items = new \ArrayObject();
    }

    public function getAsJsonLd(): string
    {
        $arr = [];
        $i = 0;

        foreach ($this->items as $item) {
            /* @var $item Item */

            $arr[] = [
                "@type" => "ListItem",
                "position" => ++$i,
                "name" => $item->getName(),
                "item" => $item->getPath()
            ];
        }

        return json_encode([
            "@context" => "https://schema.org",
            "@type" => "BreadcrumbList",
            "itemListElement" => $arr
        ]);
    }

    public function getAsHtml(): string
    {
        return '<script type="application/ld+json">' . $this->getAsJsonLd() . '</script>';
    }
}
