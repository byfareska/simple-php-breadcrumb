<?php
/**
 * Created by PhpStorm.
 * User: eska
 * Date: 07.03.19
 * Time: 16:31
 */

namespace KarolSzarafinowski\Breadcrumb;

class Item
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $path;

    public function __construct(string $name = "", string $path = "")
    {
        $this->name = $name;
        $this->setPath($path);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        if(substr($path, 0, 8) === "https://" || substr($path, 0, 7) === "http://")
            $this->path = $path;
        else
            $this->path = 'https://' . $_SERVER['HTTP_HOST'] . $path ;
    }

}
